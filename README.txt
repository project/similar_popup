-- SUMMARY --

Module shows similar nodes in a pop-up window in the bottom right
corner when the user scrolls the current page to the end of the node.

-- REQUIREMENTS --

http://drupal.org/project/similar

-- CONTACT --

Current maintainer:
* Harbuzau Yauheni - http://drupal.org/user/2123020
* Andrew Podlubnyj - http://drupal.org/user/2062438
